var application = require("application");

module.exports = {
    saveImageFromURL: function(url,first,res,slideshowname) 
    {
        var h = res.height;
        var w = res.width;
        var context = application.android.context;
        var slideshownative = new com.markf.mark.imageslidenativeservice.Slideshownative();
        var imageResult = slideshownative.downloadImage(context,url,slideshowname,first.toString(),h,w);
        console.log(imageResult);
        return(JSON.parse(imageResult));
        //alert(imageResult);
    },
    createVideoFile: function(fullSlideShow){
        var context = application.android.context;
        var slideshownative = new com.markf.mark.imageslidenativeservice.Slideshownative();
        var videoResult = slideshownative.createVideo(context,JSON.stringify(fullSlideShow));
        return videoResult;
    }
};